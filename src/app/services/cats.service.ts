import { Injectable } from '@angular/core';
import { apiCat } from 'src/app/endpoints/endpoints';
import { Cats } from '../interfaces/cats';

@Injectable({
  providedIn: 'root'
})
export class CatsService {

  constructor() { }

  async traerGatos(apiCatURL:any,apiKey:any){
    var cabeceras= new Headers({'x-api-key':apiKey});
    cabeceras.append("Content-Type","application/json")
    const response= await fetch(apiCatURL,{
      method:'GET',
      headers:cabeceras,
      redirect:'follow'
    })
    const datai=await response.json()
    return datai
  }

  async traerImagenes(apiCatURL:any,apiKey:any,imgId:string){
    var cabeceras= new Headers({'x-api-key':apiKey});
    cabeceras.append("Content-Type","application/json")
    const response= await fetch(apiCatURL+imgId,{
      method:'GET',
      headers:cabeceras,
      redirect:'follow'
    })
    const imageCat=await response.json()
    return imageCat
  }
}
