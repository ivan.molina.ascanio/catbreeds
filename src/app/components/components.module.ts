import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatsComponent } from './cats/cats.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [
    CatsComponent
  ],
  imports: [
    CommonModule,
    IonicModule.forRoot()
  ],

  exports:[
      CatsComponent
  ]
})
export class ComponentsModule { }
