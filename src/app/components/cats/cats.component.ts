import { Component, OnInit } from '@angular/core';
import { apiCat } from 'src/app/endpoints/endpoints';
import { Cats } from 'src/app/interfaces/cats';
import { CatsService } from 'src/app/services/cats.service';
@Component({
  selector: 'app-cats',
  templateUrl: './cats.component.html',
  styleUrls: ['./cats.component.scss'],
})

export class CatsComponent  implements OnInit {
  public catsArray:any=[]
  constructor(private traerGatos:CatsService) {
   this.getCats()
   }

  getCats(){
    this.traerGatos.traerGatos(apiCat.url,apiCat.apikey).then(data=>{
      for(let i of data){
        if(i.reference_image_id!=undefined){
          this.traerGatos.traerImagenes(apiCat.urlImage,apiCat.apikey,i.reference_image_id).then(datax=>{
            const dataCat={
              name:i.name,
              origin:i.origin,
              affection_level:i.affection_level,
              intelligence:i.intelligence,
              imageUrl:datax.url
             
            }
            this.catsArray.push(dataCat)
          })
        }
       
        
      }
    })
  }
  ngOnInit() {}
  

}
