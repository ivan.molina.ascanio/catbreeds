export interface Cats {
    name:string,
    origin:string,
    affection_level:Number,
    intelligence:Number,
    reference_image_id:string
}
